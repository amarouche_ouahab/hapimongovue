const request = require('request');

class ExportService {
    exportCsv(){
        return new Promise((resolve, reject) => {
            try {
                request('http://localhost:8082/exportToCsv', { json: true }, (err, res, body) => {
                    if (err) { return console.log(err); }
                    resolve(body)
                    });
                }
                catch(e) {
                    reject('connexion api faile');
                }
        });
    }
}

module.exports = new ExportService();