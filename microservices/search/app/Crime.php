<?php
    namespace App;

    use Jenssegers\Mongodb\Eloquent\Model as Moloquent;
    use Illuminate\Support\Facades\DB;

    class Crime extends Moloquent {

    protected $connection = 'mongodb';
    protected $collection = 'crime_reports'; 
    protected $fillable = [
        'compnos', 'naturecode', 'incident_type_description', 'main_crimecode',
        'reptdistrict', 'reportingarea', 'fromdate', 'weapontype',
        'shooting', 'domestic', 'shift', 'year',
        'month', 'day_week', 'ucrpart', 'x',
        'y', 'streetname', 'xstreetname', 'location',
    ];

    public function add($data)
    {
        if (!empty($data)) {
            return DB::collection('crime_reports')->insert([$data]);
        }
        return "Veuilez remplir le formulaire pour ajouter un crime!";

    }

}