<?php

namespace App\Http\Controllers;

use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Search;
use App\Crime;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {}
    
    public function getByType($type)
    {
        $reports = DB::collection('crime_reports')->where('incident_type_description', $type)->get();

        return response()->json($reports);
    }

    public function add(Request $request, Crime $crime)
    {
        $data = $request->all();
        $res = $crime->add($data);   

        return response()->json($res);
    }

    public function getAll(Request $request)
    {
        $limit = isset($request->limit) ? intval($request->limit) : 10;
        $offset = isset($request->offset) ? intval($request->offset) - 1 : 0;
        
        $data = Crime::skip($offset)->take($limit)->where('incident_type_description', '=', 'VANDALISM')->get();
        return response()->json($data);
    }

    public function search(Request $request)
    {
        $limit = isset($request->limit) ? intval($request->limit) : 10;
        $offset = isset($request->offset) ? intval($request->offset) - 1 : 0;

        $value = $request->value;

        $data = Crime::skip($offset)->take($limit)
        ->Orwhere('compnos', '=', $value)
        ->Orwhere('naturecode', '=', $value)
        ->Orwhere('incident_type_description', '=', $value)
        ->Orwhere('main_crimecode', '=', $value)
        ->Orwhere('reptdistrict', '=', $value)
        ->Orwhere('romdate', '=', $value)
        ->Orwhere('weapontype', '=', $value)
        ->Orwhere('shooting', '=', $value)
        ->Orwhere('domestic', '=', $value)
        ->Orwhere('shift', '=', $value)
        ->Orwhere('year', '=', $value)
        ->Orwhere('month', '=', $value)
        ->Orwhere('day_week', '=', $value)
        ->Orwhere('ucrpart', '=', $value)
        ->Orwhere('streetname', '=', $value)
        ->Orwhere('xstreetname', '=', $value)
        ->get();
        return response()->json($data);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $res = Crime::destroy($id);

        if ($res) {
            return response()->json($res);
        }
        return response()->json('Error: ' . $res);
    }
}