const environment = {
  API_URL: 'https://localhost:3030'
}
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'client',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script:[
      {src:'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'},
      {src:'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'},
      // {src:'@/assets/js/sb-admin.js'},
      // {src:'@/assets/js/sb-admin.min.js'},
      // {src:'@/assets/datatables/jquery.dataTables.js'},
      // {src:'@/assets/jquery-easing/jquery.easing.min.js'},
      // {src:'@/assets/datatables/dataTables.bootstrap4.js'},
      // {src:'@/assets/jquery/jquery.min.js'},
      {src:'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'},
      {src:'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'},
      {src:'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'},
      {src:'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js'},
      {src:'https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js'},
      {src:'https://code.jquery.com/jquery-3.3.1.min.js'}
    ]
  },
  /*
  ** Customize the progress bar color
  */

  css:['\~/node_modules/bootstrap/dist/css/bootstrap.css','@/assets/css/sb-admin.css','@/assets/css/sb-admin.min.css'],
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vondor:['@/assets/js/sb-admin.js','@/assets/js/sb-admin.min.js'],
  },
  axios: {
    baseURL: environment.API_URL,
    credentials : false,
    proxy: false,
    debug: true,
    retry: {
      retries: 3
    },
    requestInterceptor: (config, {store}) => {
      config.headers.common['Authorization'] = '';
      config.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;application/json';
      return config
    }
  },
}

